# Starting the Main Project

The main project can be developed, either by a single student, or as a group of max. 3 students. For many students it is an advantage to work in groups, where you have the possibility to support each other both professionally and technically. 
Please pass the following inform the internship coordinator, as early as possible, in case you hand in as a group:

1. Which students will hand in as a group
2. Which company
3. Who is your supervisor(s), both form UCL and the company
4. What is the subject(s) of the main project.

To The main project top need to be approved by the supervisor in advance. A good project statement is paramount for this step
The topic should be focus on the curriculum and it is advisable to be an extension from the internship. 



## Evaluation of the Main Project

You are encouraged to ask the company to give a written opinion. You may use this statement as a "recommendation" for future job search/applications.

An individual external exam (ie with the participation of an external examiner/censor) is held in the main project - written and oral. The exam evaluates the main project report, including the degree of achievement of the education's exit level.


## Main Project Report

The main project report should be written during, or parallel to, the work with the main project assignment. For limitations on the report as well as min and max page count see curriculum.


The report must be prepared in accordance with the standard applicable to the study, including:

1. Job Title, 
2. Company, 
3. Contact Person in the Company, 
4. Supervisor and Group Members. 
5. The project title which is going to be on the diploma. 

If you have questions about the appearance of the report, contact your supervisor at the UCL.

### Company Report template vs UCL template

In case your report is to be used in the production context of the company, it is perfectly advisable to use the company's reporting standard.

### Confidential information

If the report contains confidential information, this must be clearly stated on the cover page. The supervisor much be informed at the beginning of the project.

### Contributors

Each participant's individual contribution to the project must be stated in the report. For source code, configurations and text, it must be clear what the students themselves have written and what is obtained from other sources. Sourcing is very important.

## Hand-in

The main subject report is delivered in WISEflow on the specified date that is approx. 14 days before the defense of the thesis. Appendix, and other material can be also uploaded on WiseFlow

### Location, dates, and schedulare 

Relevant exam information can be seen on Wiseflow when the flow of given exam opens. As well as in the exam catalog.


## Evaluation of Thesis

This is an ”external” exam and the evaluation is performed by the censor and the supervisor. The exam is evaluated with a grade according to the 12-step scale. 
The Main Project will be evaluated according to the learning goals stated in the curriculum.

### Re-Examination

If you do not pass the exam (combined evaluation of the report and the oral exam), you will have to do a re-exam. As for other tests and exams, you are eligible to 2 re-exams. The foundation for the re-exam is based on a professional evaluation. It can be by establishing a brand new Main Project or do another hand-in of an edited project report, or you can be granted a new oral exam, based on the original report. In any case, this will be decided by the UCL supervisor.

# Registration

Your internship and final project need to be registered and approved. 

## Final Thesis
The final Thesis is registered with your supervisor. You contact her/him via email with a problem statement. The problem statement is something you will work on at the company that you had your internship or a topic you would like to work. If you are not continuing at the said company. Otherwise your supervisor, can come with a topic that he or she wish you to work on. 


## Template Example

A template report can be found on this website. Students are advice to have a look at it. Furthemore students are strongly adviced to use their supervisor for the report planing and writting phase.

